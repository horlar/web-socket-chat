package com.intellisense.springboot.websocket.model;

public enum MessageType {

    CHAT,
    JOIN,
    LEAVE
}
